definition module cachingcompiler;

:: *Thread :== Int;
start_caching_compiler :: !{#Char} !Thread -> (!Int,!Thread);
// int start_caching_compiler (CleanCharArray compiler_path);
start_caching_compiler_with_args :: !{#Char} !Int !Int -> Int;
// int start_caching_compiler_with_args (CleanCharArray cocl_path,char* cocl_argv,int cocl_argv_size);
call_caching_compiler :: !{#Char} !Thread -> (!Int,!Thread);
// int call_caching_compiler (CleanCharArray args);
stop_caching_compiler :: !Thread -> (!Int,!Thread);
// int stop_caching_compiler ();
