implementation module cachingcompiler;

import code from "cachingcompiler_c.";

:: *Thread :== Int;

start_caching_compiler :: !{#Char} !Thread -> (!Int,!Thread);
start_caching_compiler a0 a1 = code {
	ccall start_caching_compiler "s:I:p"
}
// int start_caching_compiler (CleanCharArray compiler_path);

start_caching_compiler_with_args :: !{#Char} !Int !Int -> Int;
start_caching_compiler_with_args a0 a1 a2 = code {
	ccall start_caching_compiler_with_args "spI:I"
}
// int start_caching_compiler_with_args (CleanCharArray cocl_path,char* cocl_argv,int cocl_argv_size);

call_caching_compiler :: !{#Char} !Thread -> (!Int,!Thread);
call_caching_compiler a0 a1 = code {
	ccall call_caching_compiler "s:I:p"
}
// int call_caching_compiler (CleanCharArray args);

stop_caching_compiler :: !Thread -> (!Int,!Thread);
stop_caching_compiler a0 = code {
	ccall stop_caching_compiler ":I:p"
}
// int stop_caching_compiler ();
